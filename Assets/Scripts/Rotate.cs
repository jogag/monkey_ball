using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    [SerializeField] private Vector3 Direction = new Vector3(0, 0, 1);
    [SerializeField] private float Speed = 2.0f;
    void Start()
    {
        
    }

   void Update()
   {
        transform.Rotate(Direction * Speed * Time.deltaTime);
   }
}

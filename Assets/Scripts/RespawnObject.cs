using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnObject : MonoBehaviour
{
    [SerializeField] private int m_Speed = 1;
    [SerializeField] private float m_MaxLimit = 5.0f;

    private Vector3 m_InitialPosition;
    private Vector3 newPosition;

    private void Start()
    {
        m_InitialPosition = gameObject.transform.position;
    }

    void Update()
    {
        newPosition = transform.position;
        newPosition.z -= m_Speed * Time.deltaTime;
        transform.position = newPosition;

        if (gameObject.transform.position.z <= m_InitialPosition.z - m_MaxLimit)
        {
            gameObject.transform.position = m_InitialPosition;
        }
    }
}

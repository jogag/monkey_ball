using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowTextHighScore : MonoBehaviour
{
    [SerializeField] PlayerMovement m_Player;

    private void Start()
    {
        int highScore = 0;
        if (PlayerPrefs.HasKey("HighScore"))
        {
            highScore = PlayerPrefs.GetInt("HighScore");
        }
        GetComponent<Text>().text = "High score : " + highScore;
    }
}

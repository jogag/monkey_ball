using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCam : MonoBehaviour
{
    [SerializeField] GameObject Player;
    [SerializeField] Vector3 Offset = new Vector3(0, -1, 5);
    Vector3 PositionPlayer;
    void Start()
    {
        PositionPlayer = Player.transform.position;
    }

    
    void Update()
    {
        transform.position = Player.transform.position - Offset;
    }
}

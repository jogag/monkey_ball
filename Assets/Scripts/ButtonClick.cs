using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonClick : MonoBehaviour
{
    [SerializeField] PlayerMovement m_Player;
    [SerializeField] SceneManagement m_Start;
    [SerializeField] SceneManagement m_Exit;
    [SerializeField] SceneManagement m_ShowScore;

    private AudioSource m_AudioS;

    private void Start()
    {
        m_AudioS = GetComponent<AudioSource>();
    }
    public void OnClickStart()
    {
        m_AudioS.Play();
        m_Player.ResetBurgerCount();
        m_Start.ToNextScene();
    }

    public void OnClickQuit()
    {
        m_AudioS.Play();
        m_Exit.QuitGame();
    }

    public void OnClickShowScore()
    {
        m_AudioS.Play();
    }
}

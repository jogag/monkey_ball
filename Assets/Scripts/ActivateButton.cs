using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateButton : MonoBehaviour
{
    [SerializeField] MovingObjectX m_Wall;
    private bool m_Activated = false;

    private void Update()
    {
        if(m_Activated)
        {
            gameObject.GetComponent<Renderer>().material.color = new Color(0, 255, 0);
            m_Wall.ChangeSpeed();
        }
    }
    public void Activated()
    {
        m_Activated = true;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowText : MonoBehaviour
{
    [SerializeField] private PlayerMovement m_textSpeed;

    private void Update()
    {
        GetComponent<Text>().text = "Speed : " + m_textSpeed.GetCurrentSpeed().ToString();
    }
}

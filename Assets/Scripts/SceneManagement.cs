using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagement : MonoBehaviour
{
    private int m_currScene;
    private int m_nextScene;
    private int m_delayTime = 2;

    private AudioSource m_AudioS;
    private PlayerMovement m_Score;

    [SerializeField] private PlayerMovement m_Player;

    private void Start()
    {
        m_AudioS = GetComponent<AudioSource>();
        m_currScene = SceneManager.GetActiveScene().buildIndex;
        m_nextScene = m_currScene + 1;
        if (m_nextScene == SceneManager.sceneCountInBuildSettings)
        {
            m_nextScene = 0;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            m_AudioS.Play();
            Invoke("ToNextScene", m_delayTime);
        }
    }

    public void ToNextScene()
    {
        if(m_nextScene == 0)
        {
            if(PlayerPrefs.HasKey("HighScore"))
            {
                int oldScore = PlayerPrefs.GetInt("HighScore");

                if(m_Player.GetBurgersQty() > oldScore)
                {
                    PlayerPrefs.SetInt("HighScore", m_Player.GetBurgersQty());
                }
            }
            else
            {
                PlayerPrefs.SetInt("HighScore", m_Player.GetBurgersQty());
            }
        }
        SceneManager.LoadScene(m_nextScene);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}

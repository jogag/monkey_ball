using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody m_Rigidbody;

    private Vector3 rayOrigin;
    private Vector3 rayDirection = new Vector3(0, -1, 0);
    private Vector3 m_Jump;
    private bool m_Grounded = true;
    private bool m_DoubleJump = false;
    private float m_CurrSpeed;
    private static int m_CurrBurgersQty;

    private AudioSource m_AudioS;
   
    [SerializeField] private float m_VerticalForce = 20.0f;
    [SerializeField] float m_Speed = 3.0f;
    [SerializeField] private Vector3 m_SpawnPosition;
    [SerializeField] private float m_limitY;
    [SerializeField] ActivateButton m_Button;

    

    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_AudioS = GetComponent<AudioSource>();
        m_Jump = new Vector3(0.0f, 1.0f, 0.0f);

        if (PlayerPrefs.HasKey("HighScore"))
        {
            m_CurrBurgersQty = PlayerPrefs.GetInt("HighScore");
        }
    }

    void Update()
    {      
        float HorizontalInput = Input.GetAxis("Horizontal");
        float VerticalInput = Input.GetAxis("Vertical");
        m_CurrSpeed = GetComponent<Rigidbody>().velocity.magnitude;
        Ray ray = new Ray(rayOrigin, rayDirection);
        RaycastHit rayInfo;
        rayOrigin = gameObject.transform.position;
        
        if (transform.position.y < m_limitY)
        {
            m_AudioS.Play();
            transform.position = m_SpawnPosition;
        }
        
        Vector3 calculatedVelocity = m_Rigidbody.velocity;
        calculatedVelocity.x = HorizontalInput * m_Speed;
        calculatedVelocity.z = VerticalInput * m_Speed;
        m_Rigidbody.velocity = calculatedVelocity;

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(m_Grounded)
            {
                m_Grounded = false;
                m_Rigidbody.AddForce(m_Jump * m_VerticalForce, ForceMode.Impulse);
            }
            else if (!m_DoubleJump && !m_Grounded)
            {
                m_DoubleJump = true;
                m_Rigidbody.AddForce(m_Jump * m_VerticalForce, ForceMode.Impulse);
            }
        }

        if (Physics.Raycast(ray, out rayInfo, 2.0f))
        {
            if (rayInfo.collider.tag == "Button")
            {
                m_Button.Activated();
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        m_Grounded = true;
        m_DoubleJump = false;
    }

    public float GetCurrentSpeed()
    {
        if (m_CurrSpeed <= 0)
        {
            return 0;
        }
        return m_CurrSpeed;
    }

    public int GetBurgersQty()
    {
        return m_CurrBurgersQty;
    }

    public void IncrementBurgersQty()
    {
        m_CurrBurgersQty++;
    }

    public void ResetBurgerCount()
    {
        m_CurrBurgersQty = 0;
    }
}


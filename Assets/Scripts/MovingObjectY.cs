using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObjectY : MonoBehaviour
{
    [SerializeField] private int m_Speed = 1;
    [SerializeField] private float m_MaxLimit = 5.0f;
    [SerializeField] private float m_MinLimit = 5.0f;
    [SerializeField] private int m_CurrentDirection = 1;

    private float m_InitialPosition;
    private Vector3 newPosition;

    private void Start()
    {
        m_InitialPosition = gameObject.transform.position.y;
    }

    void Update()
    {
        newPosition = transform.position;
        newPosition.y += m_CurrentDirection * m_Speed * Time.deltaTime;
        transform.position = newPosition;

        if (newPosition.y >= m_InitialPosition + m_MaxLimit || newPosition.y <= m_InitialPosition - m_MinLimit)
        {
            m_CurrentDirection *= -1;
        }
    }
}

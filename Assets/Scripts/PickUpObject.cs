using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpObject : MonoBehaviour
{
    [SerializeField] private PlayerMovement m_CurrBurgers;
    private AudioSource m_AudioS;
    private Renderer m_ObjRend;

    private void Start()
    {
        m_AudioS = GetComponent<AudioSource>();
        m_ObjRend = gameObject.GetComponent<Renderer>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            m_AudioS.Play();
            m_ObjRend.enabled = false;
            m_CurrBurgers.IncrementBurgersQty();
            Destroy(gameObject, 0.5f);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowTextBurgers : MonoBehaviour
{
    [SerializeField] private PlayerMovement m_currBurgers;

    private void Update()
    {
        GetComponent<Text>().text = "Score : " + m_currBurgers.GetBurgersQty().ToString();
    }
}
